using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterC : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            float speed = 500f;
            float OffsetX = Input.GetAxis("Mouse X");
            this.transform.Rotate(new Vector3(0, -OffsetX, 0) * speed * Time.deltaTime, Space.World);
        }
    }

}
