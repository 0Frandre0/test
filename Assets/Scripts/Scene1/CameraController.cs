using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float x;
    public float z;
    public float MouseX;
    public float MouseY;
    public float speed = 50;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        z = Input.GetAxis("Vertical") * Time.deltaTime * speed;
        if (Input.GetMouseButton(1))
        {
            MouseX += Input.GetAxis("Mouse X");
            MouseY += Input.GetAxis("Mouse Y");
            this.transform.Translate(x, 0, z);
            this.transform.eulerAngles = new Vector3(-MouseY, MouseX, 0);
        }
    }
}
