using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerChange : MonoBehaviour
{
    public GameObject[] characters;

    public void ChangeChar(int idx)
    {
        for (int i = 0; i < characters.Length; i++)
        {
            characters[i].SetActive(i == idx);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
