using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunRotate : MonoBehaviour
{
    public GameObject pointLight;
    public float x = 90f;
    public float speed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //this.transform.Rotate(speed * Time.deltaTime,0 ,0);
        x += speed * Time.deltaTime;
        if (x >= 360) x = 0;
        if (x >= 180) pointLight.SetActive(true);
        else pointLight.SetActive(false);
        this.transform.eulerAngles = new Vector3(x,0,0);
    }
}
