using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharAnim : MonoBehaviour
{
    public Animator anim;

    public void PlayWalk(bool isWalk)
    {
        anim.SetBool("Walk", isWalk);
    }

    public void PlayGroundPunch()
    {
        anim.SetTrigger("GroundPunch");
    }

    public void PlayHit()
    {
        anim.SetTrigger("Hit");
        
    }
}
