using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharC : MonoBehaviour
{
    public float x;
    public float speed = 1f;
    CharAnim charAnim;
    public GameObject attackBox;
    public bool isMove = true;


    // Start is called before the first frame update
    void Start()
    {
        charAnim = this.transform.GetComponent<CharAnim>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        if (Input.GetMouseButtonDown(0)) charAnim.PlayGroundPunch();
    }

    void Move()
    {
        if (!isMove) return;
        x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        if (x > 0)
        {
            this.transform.eulerAngles = new Vector3(0, -90, 0);
            charAnim.PlayWalk(true);
        }
        else if (x < 0)
        {
            this.transform.eulerAngles = new Vector3(0, 90, 0);
            charAnim.PlayWalk(true);
        }
        else
        {
            charAnim.PlayWalk(false);
        }
        this.transform.Translate(-x, 0, 0, Space.World);
    }

    public void OnAttackBox()
    {
        this.attackBox.SetActive(true);
    }

    public void OffAttackBox()
    {
        this.attackBox.SetActive(false);
    }

    public void OnMove()
    {
        this.isMove = true;
    }

    public void OffMove()
    {
        this.isMove = false;
    }
}
