using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Scene2;
using UnityEngine.UI;

public class EnemyC : MonoBehaviour
{
    private Rigidbody rb;
    public EnemyAnim anim;
    public Transform player;
    public Transform target;
    public GameObject GameOver;

    float lockDis;
    float attackDis;
    public float blanklyTime;
    float hitTime;
    float attackTime;
    float moveSpeed;
    float dieTime;
    public float Hp;

    public enum EnemyState
    {
        idle,
        walk,
        attack,
        blankly,
        hit,
        die,
    }
    public EnemyState state;

    // Start is called before the first frame update
    void Start()
    {
        lockDis = EnemyAttribute.lockDis;
        attackDis = EnemyAttribute.attackDis;
        blanklyTime = EnemyAttribute.blanklyTime;
        hitTime = EnemyAttribute.hitTime;
        moveSpeed = EnemyAttribute.moveSpeed;
        attackTime = EnemyAttribute.attackTime;
        Hp = EnemyAttribute.MaxHp;
        dieTime = EnemyAttribute.dieTime;

        this.rb = this.GetComponent<Rigidbody>();
        this.anim = this.GetComponent<EnemyAnim>();
    }

    // Update is called once per frame
    void Update()
    {
        if (state == EnemyState.idle)
        {
            this.anim.PlayWalk(false);
            if (Vector3.Distance(player.position, this.transform.position) <= lockDis)
            {
                target = player;
                state = EnemyState.walk;
            }
        }
        else if (state == EnemyState.walk)
        {
            this.transform.LookAt(target);
            this.anim.PlayWalk(true);
            this.transform.position = Vector3.MoveTowards(this.transform.position, target.position, moveSpeed * Time.deltaTime);
            if (Vector3.Distance(player.position, this.transform.position) <= attackDis)
            {
                this.anim.PlayWalk(false);
                this.anim.Attack();
                state = EnemyState.attack;
            }
        }
        else if (state == EnemyState.attack)
        {
            attackTime -= Time.deltaTime;
            
            if (attackTime <= 0)
            {                
                state = EnemyState.blankly;
                attackTime = EnemyAttribute.attackTime;
            }
        }
        else if (state == EnemyState.blankly)
        {
            blanklyTime -= Time.deltaTime;
            if (blanklyTime <= 0)
            {
                state = EnemyState.idle;
                blanklyTime = EnemyAttribute.blanklyTime;
            }
        }
        else if (state == EnemyState.hit)
        {
            anim.Hit();
            state = EnemyState.idle;
        }
        else if (state == EnemyState.die)
        {
            if(dieTime == EnemyAttribute.dieTime)
                anim.Death();
            dieTime -= Time.deltaTime;
            if (dieTime <= 0)
            {
                Destroy(this.gameObject);
                GameOver.SetActive(true);
            }
        }
    }

    public void beHit()
    {
        if (Hp <= 0) state = EnemyState.die;
        else state = EnemyState.hit;
    }
}
