using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraC : MonoBehaviour
{
    public Transform Owner;
    public float OffsetZ = 5f;
    public float OffsetY = 3f;
    public Vector3 Rotation = new Vector3(20, 180, 0);

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        this.transform.position = this.Owner.position + new Vector3(0, OffsetY, OffsetZ);
        this.transform.eulerAngles = Rotation;
    }
}
