using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIName : MonoBehaviour
{
    public Transform Owner;
    public float height = 2.2f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Owner != null)
            this.transform.position = Owner.position + Vector3.up * height;
        if (Camera.main != null)
            this.transform.forward = Camera.main.transform.forward;
    }
}
