﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Scene2
{
    public static class EnemyAttribute
    {
        public static float lockDis = 5f;
        public static float attackDis = 1f;
        public static float blanklyTime = 1f;
        public static float hitTime = 2f;
        public static float moveSpeed = 1f;
        public static float attackTime = 1f;
        public static float MaxHp = 30;
        public static float dieTime = 5f;
    }
}
