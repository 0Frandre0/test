using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBox : MonoBehaviour
{
    public float atk = 10f;

    public void OnTriggerEnter(Collider other)
    {
        EnemyC c = other.GetComponent<EnemyC>();
        c.Hp -= atk;
        c.beHit();        
    }

}
